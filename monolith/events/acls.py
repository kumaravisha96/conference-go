import requests
import json
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY


def get_photo(city, state):
    headers = {
        "Authorization": PEXELS_API_KEY,
    }
    url = "https://api.pexels.com/v1/search"
    params = {
        "query": str(city) + ", " + str(state),
    }
    res = requests.get(url, headers=headers, params=params)
    content = json.loads(res.content)
    picture_url = content["photos"][0]["src"]["original"]
    try:
        return {"picture_url": picture_url}
    except:
        return {"picture_url": None}


def get_weather_data(city, state):
    geourl = "http://api.openweathermap.org/geo/1.0/direct"
    params = {
        "q": f"{city},{state},US",
        "appid": OPEN_WEATHER_API_KEY,
    }
    res = requests.get(geourl, params=params)
    data = json.loads(res.content)
    # see if the data list has any elements (if len(data) > 0:) before trying to access its first element. If the data list is empty, we return None instead of trying to access its elements
    if len(data) > 0:
        lat = str(data[0]["lat"])
        lon = str(data[0]["lon"])

        url = "https://api.openweathermap.org/data/2.5/weather"
        params2 = {
            "lat": lat,
            "lon": lon,
            "appid": OPEN_WEATHER_API_KEY,
            "units": "imperial",
        }
        res2 = requests.get(url, params=params2)
        weatherdata = json.loads(res2.content)
        temp = str(weatherdata["main"]["temp"])
        description = weatherdata["weather"][0]["description"]
        return {
            "temperature": temp,
            "description": description,
        }
    else:
        return None
