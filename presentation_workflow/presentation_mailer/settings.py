DEBUG = True
ALLOWED_HOSTS = []
LANGUAGE_CODE = "en-us"
TIME_ZONE = "UTC"
USE_I18N = True
USE_TZ = True

EMAIL_HOST = "mail"
EMAIL_SUBJECT_PREFIX = "yo yo yo"

# This Django project has been stripped down
# to its bare minimum.
# That means it'll run faster when we want
# to process emails.
